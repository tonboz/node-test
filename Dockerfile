FROM node:12

WORKDIR /usr/src/app/

COPY index.js \
     package.json \
     package-lock.json ./

ENV NODE_ENV production

RUN npm install

EXPOSE 7000

CMD ["npm", "start"]
