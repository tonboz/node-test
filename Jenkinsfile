pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/master']], userRemoteConfigs: [[url: 'http://git.firma.hr/node-test']]])
            }
        }

        stage('Prepare') {
            steps {
                script {
                    def appName = "node-test"
                    def appSha = sh(script:'git rev-parse --short master', returnStdout:true)
                    def dockerTag = 'reg.firma.com/' + appName + ':' + appSha
                    def dockerTestTag = appName + '-test:' + appSha
                }
            }
        }

        stage('Build') {
            steps {
                script {
                    def dockerImage = docker.build(dockerTag)
                }
            }
        }

        stage('Test') {
            steps {
                script {
                    def dockerTestImage = docker.build(dockerTestTag, "-f Dockerfile.test --build-arg DOCKER_IMAGE=${dockerTag}")
                    def testResult = sh(script:"docker run --rm ${dockerTestTag}", returnStatus:true)
                    if(testResult == 0) {
                        currentBuild.result = 'UNSTABLE'
                    }
                }
            }
        }

        stage('Push') {
            when {
                expression {
                    currentBuild.result != 'UNSTABLE'
                }
            }
            steps {
                script {
                    dockerImage.push()
                }
            }
        }

        stage('Deploy') {
            when {
                expression {
                    currentBuild.result != 'UNSTABLE'
                }
            }
            steps {
                sh("DOCKER_IMAGE=${dockerTag} docker-compose --host 'ssh://deploy@server.hr' up -d")
            }
        }
    }

    post {
        unstable {
            emailext(recipientProviders: [culprits()], to: 'developers@firma.hr', subject: '$DEFAULT_SUBJECT', body: '$DEFAULT_CONTENT')
        }
        failure {
            emailext(recipientProviders: [culprits()], to: 'developers@firma.hr', subject: '$DEFAULT_SUBJECT', body: '$DEFAULT_CONTENT')
        }
    }
}
